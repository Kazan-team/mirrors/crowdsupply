SRC = updates
SOURCES = $(shell find $(SRC) -name "*.mdwn")
PDFS = $(SOURCES:$(SRC)/%.mdwn=%.pdf)

all: $(PDFS)

debug:
	$(info SOURCES is $(SOURCES))
	$(info PDFS is $(PDFS))

%.pdf:$(SRC)/%.mdwn
	pandoc -s $< -o pdfs/$@

clean:
	rm -rf *.pdf

.PHONY: all clean debug
